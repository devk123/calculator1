package com.company;


import java.util.Scanner;

public class UserInterface
{
    private Scanner sc = new Scanner(System.in);
    private Calculator calculators=null;
    private int n, value,i,choice;
    private int[] a=new int[100];
    private String choice2;

    public void displayMenuAndInputData()
    {
        do {
            System.out.println("1.Normal Calculator\n2.Magic Calculator\nEnter the calculator you want to use");
            choice = sc.nextInt();
            // This is the right way of doing it. Good
            calculators=CalculatorFactory.getCalculator(choice);
            System.out.println("How many digits?:");
            n = sc.nextInt();
            System.out.println("Enter the digits");
            for(i=0;i<n;i++)
                a[i]=sc.nextInt();
            System.out.println("1.Sum\n2.Subtract\nWhat do you want to do");
            choice = sc.nextInt();
            if (choice==1)
            {

                value = calculators.sum(a,n);
                System.out.println("Value is" + value);
            } else if (choice==2)
            {

                value = calculators.subtract(a, n);
                System.out.println("Value is" + value);
            }
            System.out.println("Do you wish to continue Y/N");
            choice2=sc.next();
        }
        while (choice2.equals("Y")||choice2.equals("y"));
    }
}